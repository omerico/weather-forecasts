const STATES = {
    'Alabama': 'AL',
    'Alaska': 'AK',
    'American Samoa': 'AS',
    'Arizona': 'AZ',
    'Arkansas': 'AR',
    'California': 'CA',
    'Colorado': 'CO',
    'Connecticut': 'CT',
    'Delaware': 'DE',
    'District Of Columbia': 'DC',
    'Federated States Of Micronesia': 'FM',
    'Florida': 'FL',
    'Georgia': 'GA',
    'Guam': 'GU',
    'Hawaii': 'HI',
    'Idaho': 'ID',
    'Illinois': 'IL',
    'Indiana': 'IN',
    'Iowa': 'IA',
    'Kansas': 'KS',
    'Kentucky': 'KY',
    'Louisiana': 'LA',
    'Maine': 'ME',
    'Marshall Islands': 'MH',
    'Maryland': 'MD',
    'Massachusetts': 'MA',
    'Michigan': 'MI',
    'Minnesota': 'MN',
    'Mississippi': 'MS',
    'Missouri': 'MO',
    'Montana': 'MT',
    'Nebraska': 'NE',
    'Nevada': 'NV',
    'New Hampshire': 'NH',
    'New Jersey': 'NJ',
    'New Mexico': 'NM',
    'New York': 'NY',
    'North Carolina': 'NC',
    'North Dakota': 'ND',
    'Northern Mariana Islands': 'MP',
    'Ohio': 'OH',
    'Oklahoma': 'OK',
    'Oregon': 'OR',
    'Palau': 'PW',
    'Pennsylvania': 'PA',
    'Puerto Rico': 'PR',
    'Rhode Island': 'RI',
    'South Carolina': 'SC',
    'South Dakota': 'SD',
    'Tennessee': 'TN',
    'Texas': 'TX',
    'Utah': 'UT',
    'Vermont': 'VT',
    'Virgin Islands': 'VI',
    'Virginia': 'VA',
    'Washington': 'WA',
    'West Virginia': 'WV',
    'Wisconsin': 'WI',
    'Wyoming': 'WY'
}
const app = angular.module('WeatherForecast', [])

.factory('APIFactory', function( $http, $q ) {
    const API_URL = 'http://localhost/weissberger/api'
    
    return {
        getWeather: args => {
            return $http.get(`${API_URL}/forecast/?days=${args.days}&state=${args.state}&city=${args.city}`)
        },

        searchLocation: input => {
            let defer = $q.defer()

            $http.get(`${API_URL}/states/?query=${input}`).then(response => {
                let results = response.data.RESULTS

                let suggestions = []

                angular.forEach(results, function( value ) {
                    let loc = value.name.split(', ')
                    suggestions.push({
                        name: loc[0],
                        state: STATES[ loc[1] ],
                        full: loc.join(', ')
                    })
                })

                defer.resolve( suggestions )
            })

            return defer.promise
        }
    }
})

.filter('state', () => {
    return input => {
        return input.split('. ')[0]
    }
})

.filter('shortdate', () => {
    return input => {
        return input.split('on')[1].trim()
    }
})
    
.controller('MainController', function( $scope, APIFactory, $timeout ) {
    this.forecast = null
    this.location_chosen = null
    this.days = 1
    this.suggestions = []
    this.loading = false
    this.mode = 'forecast'
	this.date = ''

    let countDays = date => {
        let oneDay = 1000 * 60 * 60 * 24
        return Math.ceil( ( new Date( date ).getTime() - new Date().getTime() ) / oneDay )
    }

    this.getForecast = e => {
        e.preventDefault()

		let args = {
			date: this.date,
			state: this.location_chosen.state,
			city: this.location_chosen.name.toLowerCase().replace(' ', '_')
		}

		args.days = this.mode === 'forecast' ? this.days : countDays( this.date )

		APIFactory.getWeather( args ).then(response => this.forecast = response.data)
    }

    $scope.$watch( () => this.location , ( newValue, oldValue ) => {
        if ( typeof newValue === 'undefined' )
            return

        if ( newValue !== oldValue ) {
            this.location_chosen = null
            this.suggestions = []
        }

        if ( !newValue ) {
            this.location = null
            return
        }

        this.loading = true
        APIFactory.searchLocation( newValue ).then(results => {
            this.loading = false
            this.suggestions = results
            this.location_chosen = this.suggestions[0]
        })
    })
})