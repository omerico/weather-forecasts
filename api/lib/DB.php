<?php
namespace Weather;

class DB {
    static private $_instance = null;
    
    public static function getInstance() {
        if ( self::$_instance == null )
            self::$_instance = new \mysqli('localhost', 'root', '', 'weather');
        
        return self::$_instance;
    }
}