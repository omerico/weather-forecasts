<?php
namespace Weather;

class WeatherService {
    /**
     * Allowed fields to search by
     */
    const FIELDS = ['days', 'state', 'city'];

    /**
     * Maximum days limit
     */
    const MAX_DAYS = 10;

    /**
     * @var WeatherUndergroundAPI
     */
    private $api_service;

    /**
     * WeatherService constructor.
     * Inject api service and mysqli instance
     * @param WeatherUndergroundAPI $api_service
     * @param \mysqli $db
     */
    public function __construct( WeatherUndergroundAPI $api_service, \mysqli $db ) {
        $this->api_service = $api_service;
        $this->db = $db;
    }

    /**
     * Calculates the date $days days from now
     * @param int $days
     * @return string
     */
    private function calcDateHistory( int $days ) : string {
        $countDays = abs( $days ) * -1;
        return date('Ymd', strtotime("$countDays days"));
    }

    /**
     * Converts a string to a slug (dashes and lowercase letters)
     * @param array $args
     * @return array
     */
    private static function toSlug( array $args ) : array {
        return array_map(function( $val ) {
            $slug = strtolower( $val );
            return str_replace('_', '-', $slug);
        }, $args);
    }

    /**
     * Searches in the DB by given args (state and city)
     * @param array $args
     * @param string $mode
     * @return array|bool
     */
    private function search( array $args, string $mode = 'forecast' ) {
        $newArgs = self::toSlug( $args );

        if ( $mode == 'history' ) {
            $date = self::normalizeDate( $args['date'] );
            $query = "SELECT data FROM history WHERE date='$date' AND city = '{$newArgs['city']}' AND state = '{$newArgs['state']}'";
        } else
            $query = "SELECT data FROM forecasts WHERE city = '{$newArgs['city']}' AND state = '{$newArgs['state']}'";

        $result = $this->db->query( $query );

        if ( $result->num_rows == 0 )
            return false;

        return json_decode( $result->fetch_assoc()['data'] );
    }

    /**
     * Converts any date to Y-m-d date
     * @param string $date
     * @return bool|string
     */
    private static function normalizeDate( string $date ) {
        return date('Y-m-d', strtotime( $date ));
    }

    /**
     * Saves data to the DB by the given args (state and city)
     * @param array $args
     * @param string $mode
     * @param array $data
     * @return bool
     */
    private function saveData( array $args, string $mode = 'forecast', array $data ) : bool {
        $newArgs = self::toSlug( $args );

        if ( $mode == 'history' ) {
            $date = self::normalizeDate( $newArgs['date'] );
            $query = "INSERT INTO history (date, city, state, data) VALUES ('$date', '{$newArgs['city']}', '{$newArgs['state']}', '".json_encode( $data )."')";
        } else
            $query = "INSERT INTO forecasts (city, state, data) VALUES ('{$newArgs['city']}', '{$newArgs['state']}', '".json_encode( $data )."')";

        $this->db->query( $query );
        return (bool) $this->db->affected_rows;
    }

    /**
     * Slices the array in the given limit
     * @param array $results
     * @param int $limit
     */
    private static function limitResults( array &$results, int $limit ) {
        $results = array_slice( $results, 0, $limit );
    }

    /**
     * This method checks the mode of forecast (history or not) and call to the matching method
     * from the api service
     * Finally, it filters the results by the resultls limit
     * @param array $args
     * @return array
     * @throws \Exception
     */
    public function getWeather( array $args ) {
        @$fields = array_keys( $args );

        if ( $fields != self::FIELDS )
            throw new \Exception('Arguments must include only the following fields: ' . implode(', ', self::FIELDS));

        if ( $args['days'] > self::MAX_DAYS )
            throw new \Exception('Maximum forecast period is ' . self::MAX_DAYS . ' days');
        
        $mode = $args['days'] < 0 ? 'history' : 'forecast';

        $newArgs = $args;

        if ( $mode == 'history' )
            $newArgs['date'] = $this->calcDateHistory( $args['days'] );

        // $data will be equal either to false or the actual datea
        if ( false === $data = $this->search( $newArgs, $mode ) ) {
            // If it is false, we did not find it, call the api
            $data = $this->api_service->getWeather( $newArgs, $mode );

            // And save the data
            $this->saveData( $newArgs, $mode, $data );
        }

        // Slice the results
        if ( $mode == 'forecast' )
            self::limitResults( $data, $args['days'] );
        
        return $data;
    }
}