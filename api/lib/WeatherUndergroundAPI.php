<?php
namespace Weather;

class WeatherUndergroundAPI {
    const API_ENDPOINT = 'http://api.wunderground.com/api/';
    const API_KEY = 'f2eaaccd3b47ea00';

    /**
     * @param array $args
     * @param string $forecast
     * @return string
     */
    private function buildURL( array $args, string $forecast = 'forecast' ) : string {
        if ( $forecast == 'forecast' )
            return self::API_ENDPOINT . self::API_KEY . "/forecast10day/q/{$args['state']}/{$args['city']}.json";

        return self::API_ENDPOINT . self::API_KEY . "/history_{$args['date']}/q/{$args['state']}/{$args['city']}.json";
    }

    /**
     * @param string $url
     * @return string
     */
    private function sendRequest( string $url ) : string {
        return file_get_contents( $url );
    }

    /**
     * @param string $response
     * @return mixed
     * @throws \Exception
     */
    private function parseResponse( string $response ) {
        $parsed = json_decode( $response, true );

        if ( isset( $parsed['error'] ) )
            throw new \Exception( $parsed['error']['description'] );

        if ( isset( $parsed['response']['results'] ) )
            throw new \Exception('No such city or state');

        if ( isset( $parsed['forecast'] ) )
            return $parsed['forecast'];

        return $parsed['history']['dailysummary'];
    }

    /**
     * @param array $response
     * @return array
     */
    private function parseForecast( array $response ) : array {
        $weather = [];
        $forecast['simple'] = $response['txt_forecast']['forecastday'];
        $forecast['complex'] = $response['simpleforecast']['forecastday'];

        $i = 0;
        foreach ( $forecast['complex'] as $day ) {
            $weather[ $i ] = [];
            $weather[ $i ]['date'] = $day['date']['pretty'];
            $weather[ $i ]['max_temp'] = $day['high']['celsius'];
            $i++;
        }

        for ( $j = 0, $i = 0; $i < sizeof( $forecast['simple'] ); $j++, $i += 2 ) {
            $day = $forecast['simple'][ $i ];
            $weather[ $j ]['weather_icon'] = $day['icon_url'];
            $weather[ $j ]['description'] = $day['fcttext'];
        }

        return $weather;
    }

    /**
     * @param array $response
     * @return array
     */
    private function parseHistory( array $response ) : array {
        $weather = $response[0];

        return [[
            'date' => $weather['date']['pretty'],
            'weather_icon' => 'http://wwemotors.com/_img/no-img-placeholder.png',
            'max_temp' => $weather['maxtempm'],
            'description' => 'Not Available',
        ]];
    }

    /**
     * @param array $args
     * @param string $mode
     * @return array
     * @throws \Exception
     */
    public function getWeather( array $args, string $mode ) : array {
        $url = $this->buildURL( $args, $mode );
        $response = $this->sendRequest( $url );
        $response = $this->parseResponse( $response );

        return ( $mode == 'history' ) ? $this->parseHistory( $response ) : $this->parseForecast( $response );
    }
}