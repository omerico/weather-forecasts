Weather forecast service based on WeatherUnderground API

Requirements: PHP version 7 or above, active MySQL service

**How to run:**

1. Please dump the 'DB.sql' file into the DB, it should create the DB weather and two tables
2. Go to http://localhost/weissberger
3. If you want to access the API directly please go to http://localhost/weissberger/api/forecast
4. For example, http://localhost/weissberger/api/forecast/?days=3&state=CA&city=Los_Angeles OR http://localhost/weissberger/api/forecast/?days=-21&state=CA&city=Los_Angeles for history