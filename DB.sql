CREATE DATABASE weather;
USE weather;

CREATE TABLE forecasts (
    city varchar(30) NOT NULL,
    state varchar(2) NOT NULL,
    data text NOT NULL,
    PRIMARY KEY (city, state)
)

CREATE TABLE history (
    date date NOT NULL,
    city varchar(30) NOT NULL,
    state varchar(2) NOT NULL,
    data text NOT NULL,
    PRIMARY KEY (date, city, state)
)